#include <Adafruit_Fingerprint.h>


HardwareSerial mySerial(2);

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

String STATUS_TO_STRING[
  "FINGERPRINT_OK",
  "FINGERPRINT_PACKETRECIEVEERR",
  "FINGERPRINT_NOFINGER",
  "FINGERPRINT_IMAGEFAIL",
  "",
  "",
  "FINGERPRINT_IMAGEMESS",
  "FINGERPRINT_FEATUREFAIL",
  "FINGERPRINT_NOMATCH",
  "FINGERPRINT_NOTFOUND",
  "FINGERPRINT_ENROLLMISMATCH",
  "FINGERPRINT_BADLOCATION",
  "FINGERPRINT_DBRANGEFAIL",
  "FINGERPRINT_UPLOADFEATUREFAIL",
  "FINGERPRINT_PACKETRESPONSEFAIL",
  "FINGERPRINT_UPLOADFAIL",
  "FINGERPRINT_DELETEFAIL",
  "FINGERPRINT_DBCLEARFAIL",
  "",
  "FINGERPRINT_PASSFAIL",
  "",
  "FINGERPRINT_INVALIDIMAGE",
  "",
  "",
  "FINGERPRINT_FLASHERR",
  "",
  "FINGERPRINT_INVALIDREG",
  "",
  "",
  "",
  "",
  "",
  "FINGERPRINT_ADDRCODE",
  "FINGERPRINT_PASSVERIFY"
]

bool is_reading_finger_print = false;


void setup_fingerprint() {
  finger.begin(57600);
  
  if (finger.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    while (1) { delay(1); }
  }
}

void getFingerprintEnroll(int finger_id) {

  xTaskCreatePinnedToCore([] (void *arguement) {
    while (1) {

    }
  }, "ReadingFingerPrint", 4096, NULL, 1, NULL, 1);

  int p = -1;

  while (p != FINGERPRINT_OK && is_reading_finger_print) {
    p = finger.getImage();

  }

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  Serial.println("Remove finger");

  delay(2000);

  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(id);
  p = -1;
  Serial.println("Place same finger again");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  // OK converted!
  Serial.print("Creating model for #");  Serial.println(id);
  
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("Prints matched!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.println("Fingerprints did not match");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  
  Serial.print("ID "); Serial.println(id);
  p = finger.storeModel(finger_id);
  if (p == FINGERPRINT_OK) {
    Serial.println("Stored!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
}
